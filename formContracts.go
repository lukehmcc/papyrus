package main

import "log"

// This thing goes through the list of best hosts
// then it forms new contracts if the contract count
// is insufficent

func formContracts() {
	hostListDataJSON, err := grabHosts()
	if(err != nil){
		log.Println("Grabbing hosts from siastats failed, try again later or set them manually")
		return
	}
	configJSON := grabConfig()
	contractDataJSON := grabCurrContracts()
	// form contracts
	if numCurrContracts() < getNumHosts()-10 {
		for i := 0; i < len(hostListDataJSON); i++ {
			// Check if contract had already been made with the host
			alreadyFormed := false
			for j := 0; j < len(contractDataJSON); j++ {
				if contractDataJSON[j].HostKey == hostListDataJSON[i].Pubkey {
					alreadyFormed = true
				}
			}
			if alreadyFormed != true {
				resp := formOrRenewContracts(hostListDataJSON[i].Pubkey, "", "form")
				if(resp != nil){
					log.Println(resp)
				}
			}
		}
	}
	// grab all current contractIDs and add them to the deafult set
	contractIDList := listCurrentContractIDs(configJSON.MuseAddr, "")
	// don't forget to lop of everything but the first 8 characters of each id
	// because the createHostSet function likes that for some reason
	for i := 0; i < len(contractIDList); i++ {
		contractIDList[i] = contractIDList[i][8:16]
	}
	log.Println(contractIDList)
	if len(contractIDList) != 0 {
		err := createHostSet(configJSON.MuseAddr, "default", contractIDList)
		if err != nil {
			log.Println(err)
			log.Fatal("[formContracts] creating host set failed")
		}
	}
	log.Println("[formContracts] contracts formed properly")
}
