package main

import (
  "io/ioutil"
)

// This script goes through all current files one by one and checks their host set
// against the current host set. If more than the threshold of hosts have been
// churned, it'll run the migrate function on them.

func recursiveMigrateFiles() {
  migrateVirtualFolder("/")
}
// did someone say recursive loops O:
// never actually used one of these in production before(always a good sign)
func migrateVirtualFolder(directoryName string){
  // get the home dir then loop through the rest of them
  dirDataByte, err := badgerGET(directoryName, DBLoc)
  check(err, "Retreiving the home directory failed while migrating files")
  dirData := decodeToDirData(dirDataByte)
  // get current contract set
  configJSON := grabConfig()
  currentHostIDs := listCurrentContractIDs(configJSON.MuseAddr, "")
  // check all files
  for _, o := range dirData.Files {
    fileDataByte, err := badgerGET(o, DBLoc)
    check(err, "fetching file failed in migration")
    fileData := decodeToFile(fileDataByte)
    // then go through the contracts to see if they match
    matches := 0
    for _, i := range fileData.HostIDs {
      if(arrayContainsString(i, currentHostIDs)){
        matches++
      }
    }
    // now if there are less than 75% of the hosts that are still in the set,
    // we gotta refresh it and migrate
    if(float64(matches)/float64(len(fileData.HostIDs)) < .75){
      // first we have to write the usa file to disk to be passed to the migrate funciton
      err := ioutil.WriteFile(getMetaPath()+fileData.FileName+".usa", fileData.UsaFile, 0644)
      check(err, "writing us archive failed in migrating " + fileData.FileName)
      // pass it to the migrate function
      err = migrateRemote(makeHostSet(), getMetaPath()+fileData.FileName+".usa")
      check(err, "migrating us file failed " + fileData.FileName)
      // finally, put the new us file back into the file struct and write it to badger
      usaBytes, err := ioutil.ReadFile(getMetaPath()+fileData.FileName+".usa")
      check(err, "writing .usa file failed in migration of " + fileData.FileName)
      fileData.UsaFile = usaBytes
      fileDataBytes := encodeToBytes(fileData)
      err = badgerSET(o, fileDataBytes, DBLoc)
      check(err, "setting file to badger failed in migration")
    }
  }
  // then send all the directories back up the loop so they can be gone through
  for _, i := range dirData.SubDirs {
    migrateVirtualFolder(i)
  }
}
