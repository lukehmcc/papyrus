package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/big"
	"net/http"
	"os"
	"os/user"
	"strconv"
	"strings"
	"net/textproto"
	"mime"

	"lukechampine.com/us/wallet"
)

// Returns working path
func workingPath() string {
	usr, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}
	// Make sure .config exists
	if _, err := os.Stat(usr.HomeDir + "/.config"); os.IsNotExist(err) {
		os.Mkdir(usr.HomeDir+"/.config", 0700)
	}
	// Make sure .config/papyrus exists
	if _, err := os.Stat(usr.HomeDir + "/.config/papyrus"); os.IsNotExist(err) {
		os.Mkdir(usr.HomeDir+"/.config/papyrus", 0700)
	}
	// Make sure .config/papyrus/badger exists
	if _, err := os.Stat(usr.HomeDir + "/.config/papyrus/Badger"); os.IsNotExist(err) {
		os.Mkdir(usr.HomeDir+"/.config/papyrus/Badger", 0700)
	}
	// Make sure .config/papyrus/tempUploads exists
	if _, err := os.Stat(usr.HomeDir + "/.config/papyrus/tempUploads"); os.IsNotExist(err) {
		os.Mkdir(usr.HomeDir+"/.config/papyrus/tempUploads", 0700)
	}
	return usr.HomeDir + "/.config/papyrus"
}

// returns the metadata path
func getMetaPath() string {
	userWorkingPath := workingPath()
	// Make sure .config/papyrus exists
	if _, err := os.Stat(userWorkingPath + "/metaFiles"); os.IsNotExist(err) {
		os.Mkdir(userWorkingPath + "/metaFiles", 0700)
	}
	return userWorkingPath + "/metaFiles"
}

// returns the tempPath path
func getTempPath() string {
	userWorkingPath := workingPath()
	// Make sure .config/papyrus exists
	if _, err := os.Stat(userWorkingPath + "/tempFiles"); os.IsNotExist(err) {
		os.Mkdir(userWorkingPath + "/tempFiles", 0700)
	}
	return userWorkingPath + "/tempFiles"
}

// Posts to url with given data
func fetchPost(url string, data []byte) *http.Response {
	// posts to siaststs and checks for errors
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
	if err != nil {
		print("[standard library] request to " + url + " failed")
	}
	return resp
}

// Posts to url with given data
func fetchGet(url string) *http.Response {
	// posts to siaststs and checks for errors
	resp, err := http.Get(url)
	if(err != nil){
		fmt.Println("[standard library] request to " + url + " failed")
	}
	return resp
}

// grabs config json
func grabConfig() Config {
	// Grabs the config and turns it to struct
	configDataRaw, err := ioutil.ReadFile(workingPath() + "/config.json")
	check(err, "reading config failed")
	var configDataJSON Config
	err = json.Unmarshal(configDataRaw, &configDataJSON)
	check(err, "unmarshalling config failed")
	return configDataJSON
}

func getNumHosts() int {
	// pull host number
	configJSON := grabConfig()
	// Decides how many hosts
	numHosts := int(configJSON.Storage * 4 * float32(configJSON.Redundancy))
	if numHosts < 30 {
		numHosts = 30
	} else if numHosts > 100 {
		numHosts = 100
	}
	return numHosts
}

// grabs hostList json
func grabHosts() ([]SiaStats, error) {
	numHosts := getNumHosts()
	// make host list
	resp, err := pullHosts(numHosts)
	if(err != nil){
		log.Println("pulling hosts failed so grabbing siastats struct can't work")
		var empty []SiaStats
		return empty, err
	}
	fmt.Println(resp)
	// grab host list
	hostListDataRaw, err := ioutil.ReadFile(workingPath() + "/hostList.json")
	if err != nil {
		var empty []SiaStats
		fmt.Println(err)
		return empty, err
	}
	// turn it to json
	var hostListDataJSON []SiaStats
	err = json.Unmarshal(hostListDataRaw, &hostListDataJSON)
	if err != nil {
		var empty []SiaStats
		fmt.Println(err)
		return empty, err
	}
	return hostListDataJSON, nil
}

// Turns string to bigint
func toBigInt(input string) *big.Int {
	thing := new(big.Int)
	thing, ok := thing.SetString(input, 10)
	if !ok {
		log.Fatal("Converting to big int didn't work")
		x := big.NewInt(0)
		return x
	}
	return thing
}

// Get's the height
func getHeight() int {
	config := grabConfig()
	resp, err := http.Get(config.ShardAddr + "/height")
	if err != nil {
		print("[standard library] request to " + config.ShardAddr + "/height" + " failed")
		return 0
	}
	responseData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return 0
	}
	height, err := strconv.Atoi(string(responseData))
	if err != nil {
		fmt.Println(err)
		return 0
	}
	return height
}

// finds the number of contracts
func numCurrContracts() int {
	contractDataJSON := grabCurrContracts()
	return len(contractDataJSON)
}

// finds the number of contracts
func grabCurrContracts() []ContractStruct {
	configJSON := grabConfig()
	resp := fetchGet(configJSON.MuseAddr + "/contracts")
	respData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		println("[standardLibrary] reading contracts body failed")
	}
	var contractDataJSON []ContractStruct
	err = json.Unmarshal([]byte(respData), &contractDataJSON)
	return contractDataJSON
}

// Grabs the seed
func grabDatSeed() wallet.Seed {
	configJSON := grabConfig()
	seed, err := wallet.SeedFromPhrase(configJSON.Seed)
	if err != nil {
		log.Fatal(err)
	}
	return seed
}

// grabs the parent directory
func parentDir(subDir string) string {
    if(subDir == "/"){
        return "/"
    }else{
        return subDir[:1+strings.LastIndex(subDir[:len(subDir)-1], "/")]
    }
}

// grabs file name(of a directory or a file)
func fileName(fullPath string) string {
  if(fullPath == ""){
    return ""
  }
  return fullPath[1+strings.LastIndex(fullPath[:len(fullPath)-1], "/"):]
}

// checks an error
func check(err error, errorMessage string) {
	if err != nil {
		log.Fatalln(errorMessage, err)
	}
}

// gets the mime type
func mediaTypeOrDefault(header textproto.MIMEHeader) string {
	mediaType, _, err := mime.ParseMediaType(header.Get("Content-Type"))
	if err != nil {
		return "application/octet-stream"
	}
	return mediaType
}

// contains
func arrayContainsString(item string, itemList []string) bool {
	for _, o := range itemList {
		if(item == o){
			return true
		}
	}
	return false
}
