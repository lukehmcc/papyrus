package main

import (
	"fmt"
	"log"
	"io/ioutil"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/NebulousLabs/Sia/types"
	"lukechampine.com/frand"
	"lukechampine.com/muse"
	"lukechampine.com/shard"
	"lukechampine.com/us/hostdb"
	"lukechampine.com/us/renter"
	"lukechampine.com/us/renter/proto"
	"lukechampine.com/us/renterhost"
	"lukechampine.com/us/renter/renterutil"
	"gitlab.com/NebulousLabs/Sia/modules"
)

// This is stolen code that helps out with contract
// stuff, stolen from here: https://github.com/lukechampine/muse/blob/master/cmd/musec/actions.go

// this is a host map?
type mapHKR map[hostdb.HostPublicKey]modules.NetAddress

func (m mapHKR) ResolveHostKey(hpk hostdb.HostPublicKey) (modules.NetAddress, error) {
	addr, ok := m[hpk]
	if !ok {
		return "", errors.New("no record of that host")
	}
	return addr, nil
}

// creates host set
func createHostSet(museAddr string, setName string, hostPrefixes []string) error {
	c := muse.NewClient(museAddr)
	sc := c.SHARD()
	hosts := make([]hostdb.HostPublicKey, len(hostPrefixes))
	for i := range hosts {
		var err error
		hosts[i], err = sc.LookupHost(hostPrefixes[i])
		if err != nil {
			return err
		}
	}
	err := c.SetHostSet(setName, hosts)
	if err != nil {
		return err
	}
	fmt.Printf("Created host set %q\n", setName)
	return nil
}

// checks up one hosts
func checkup(museAddr string, id string) error {
	c := muse.NewClient(museAddr)
	sc := c.SHARD()
	contracts, err := c.AllContracts()
	if err != nil {
		return err
	}
	var contract muse.Contract
	for _, contract = range contracts {
		if strings.HasPrefix(contract.ID.String(), id) {
			break
		}
	}
	if len(contract.RenterKey) == 0 {
		return errors.New("contract not found")
	}

	hostIP, err := sc.ResolveHostKey(contract.HostKey)
	if err != nil {
		return errors.Wrap(err, "could not resolve host key")
	}

	start := time.Now()
	sess, err := proto.NewSession(hostIP, contract.HostKey, contract.ID, contract.RenterKey, 0)
	if err != nil {
		return errors.Wrap(err, "could not initiate download protocol")
	}
	defer sess.Close()
	latency := time.Since(start)

	// fetch a random sector root
	numSectors := sess.Revision().NumSectors()
	if numSectors == 0 {
		return errors.New("no sectors stored on host")
	}
	roots, err := sess.SectorRoots(frand.Intn(numSectors), 1)
	if err != nil {
		return errors.Wrap(err, "could not get a sector to test")
	}
	root := roots[0]

	start = time.Now()
	err = sess.Read(ioutil.Discard, []renterhost.RPCReadRequestSection{{
		MerkleRoot: root,
		Offset:     0,
		Length:     renterhost.SectorSize,
	}})
	bandTime := time.Since(start)
	if err != nil {
		return errors.Wrap(err, "could not download sector")
	}
	bandwidth := (renterhost.SectorSize * 8 / 1e6) / bandTime.Seconds()

	fmt.Printf("OK   Host %v: Latency %0.3fms, Bandwidth %0.3f Mbps\n",
		contract.HostKey.ShortKey(), latency.Seconds()*1000, bandwidth)

	return nil
}

// Lists contracts based on passed params, this one actually was modified by me
// a tad so it returns a list of ID's instead of printing anything
func listCurrentContractIDs(museAddr, hostset string) []string {
	c := muse.NewClient(museAddr)
	var contracts []muse.Contract
	var err error
	var idList []string
	if hostset != "" {
		contracts, err = c.Contracts(hostset)
	} else {
		contracts, err = c.AllContracts()
	}
	if err != nil {
		fmt.Println(err)
		return idList
	}
	if len(contracts) == 0 {
		fmt.Println("No contracts.")
		return nil
	}
	for i := 0; i < len(contracts); i++ {
		temp := fmt.Sprintf("%s", contracts[i].ID)
		idList = append(idList, temp)
	}
	return idList
}

// renews, stolen from musec once agian
func renew(museAddr, id string, funds types.Currency, endStr string) error {
	mc := muse.NewClient(museAddr)
	sc := mc.SHARD()

	var fcid types.FileContractID
	if err := fcid.LoadString(id); err != nil {
		return err
	}

	start, err := sc.ChainHeight()
	if err != nil {
		return err
	}
	end, err := parseEnd(start, endStr)
	if err != nil {
		return err
	}

	contracts, err := mc.AllContracts()
	if err != nil {
		return err
	}
	var hostKey hostdb.HostPublicKey
	for _, c := range contracts {
		if c.ID == fcid {
			hostKey = c.HostKey
			break
		}
	}
	if hostKey == "" {
		return errors.New("no record of that contract")
	}

	settings, err := mc.Scan(hostKey)
	if err != nil {
		return err
	}
	rc, err := mc.Renew(fcid, funds, start, end, settings)
	if err != nil {
		return err
	}
	fmt.Println("Renewed contract:", rc.ID)
	return nil
}

// this is stolen from musec parse.go
func parseEnd(startHeight types.BlockHeight, end string) (types.BlockHeight, error) {
	if end == "" {
		return 0, errors.New("invalid end height or duration")
	}
	switch end[0] {
	case '@':
		intHeight, err := strconv.Atoi(end[1:])
		if err != nil {
			return 0, err
		}
		return types.BlockHeight(intHeight), nil
	default:
		intDuration, err := strconv.Atoi(end)
		if err != nil {
			return 0, err
		}
		return startHeight + types.BlockHeight(intDuration), nil
	}
}

// this is stolen from user main.go
func makeHostSet() *renterutil.HostSet {
	contracts, hkr := getContracts()
	currentHeight, err := getCurrentHeight()
	log.Println("Could not get current height:", err)
	hs := renterutil.NewHostSet(hkr, currentHeight)
	for _, c := range contracts {
		hs.AddHost(c)
	}
	return hs
}

// grabs height in the right way
func getSHARD() *shard.Client {
	config := grabConfig()
	log.Println(config.ShardAddr)
	if config.ShardAddr != "" {
		return shard.NewClient(config.ShardAddr)
	}
	return muse.NewClient(config.WalrusAddr).SHARD()
}

// grabs height in the right way
func getCurrentHeight() (types.BlockHeight, error) {
	return getSHARD().ChainHeight()
}

// gets contracts in the right way; oml I have a lot of rewritng to do
func getContracts() ([]renter.Contract, renter.HostKeyResolver) {
	config := grabConfig()
	if config.MuseAddr == "" {
		log.Fatal("Could not get contracts: no muse server specified")
	}
	c := muse.NewClient(config.MuseAddr)
	contracts, err := c.Contracts(config.HostSet)
	log.Println("Could not get contracts:", err)
	set := make([]renter.Contract, len(contracts))
	hkr := make(mapHKR, len(contracts))
	for i, c := range contracts {
		set[i] = c.Contract
		hkr[c.HostKey] = c.HostAddress
	}
	return set, hkr
}
