package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"bufio"
	"os"
)

func initConfig() string {
	// Take input
	var storage float32
	fmt.Println("What is your expected storage(in TB)(default 1TB)(Ex: .01 would be 10GB)?")
	fmt.Scanln(&storage)
	var download float32
	fmt.Println("What is your expected download(in TB)(default 1TB)(Ex: .01 would be 10GB)?")
	fmt.Scanln(&download)
	var upload float32
	fmt.Println("What is your expected upload(in TB)(default 1TB)(Ex: .01 would be 10GB)?")
	fmt.Scanln(&upload)
	var timeFrame float32
	fmt.Println("What is the timeframe for storing the data(default 3 months)(Ex: 2 would be 2 months)")
	fmt.Scanln(&timeFrame)
	var renewWindow float32
	fmt.Println("What is the renew window for the data(default 1 month)(Ex: 1 would be one month)")
	fmt.Scanln(&renewWindow)
	var redundancy int
	fmt.Println("What is your wanted redundancy?(default 3x)(Ex: 4 would be 4x redundancy)")
	fmt.Scanln(&redundancy)
	var walrusAddr string
	fmt.Println("What is walrus address?(no default as of now)(ex: 'https://narwal.lukechampine.com/wallet/<randomString>'")
	fmt.Scanln(&walrusAddr)
	var museAddr string
	fmt.Println("What is muse address?(default 'http://localhost:9560')")
	fmt.Scanln(&museAddr)
	var seed string
	fmt.Println("What is your seed?")
	// this is a weird scanner just because seeds are space sepereated unlike the other ones
	scanner := bufio.NewScanner(os.Stdin)
  if scanner.Scan() {
      seed = scanner.Text()

  }
	// if muse address is empty, fill it
	if(museAddr == "" || museAddr[:4] != "http"){
		museAddr = "http://localhost:9580"
	}
	var shardAddr string
	fmt.Println("What is shard address?(default 'http://shard.lukechampine.com/')")
	fmt.Scanln(&shardAddr)
	// if shard address is empty, fill it
	if (shardAddr == "" || shardAddr[:4] != "http") {
		shardAddr = "http://shard.lukechampine.com"
	}
	// Now write to json
	var configData Config
	// sets the data
	configData.Storage = storage
	configData.Download = download
	configData.Upload = upload
	configData.TimeFrame = timeFrame
	configData.RenewWindow = renewWindow
	configData.Redundancy = redundancy
	configData.WalrusAddr = walrusAddr
	configData.MuseAddr = museAddr
	configData.ShardAddr = shardAddr
	configData.Seed = seed
	configData.HostSet = "default" // I'll consider making this non-hardcoded in the future, but no promises
	// Now write to disk
	configDataJSON, err := json.Marshal(configData)
	if err != nil {
		fmt.Println(err)
		return "[config] marshalling json failed"
	}
	// Write that to disk
	err = ioutil.WriteFile(workingPath()+"/config.json", configDataJSON, 0644)
	if err != nil {
		fmt.Println(err)
		return "[config] io write failed"
	}
	return "[config] sucsessfully pulled config and wrote to disk"
}
