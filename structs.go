package main

// A fun little spot for all my structs

import (
	"net/textproto"
)

// HostKey is used to store the host key...
type HostKey struct {
	HostKey string `json:"HostKey"`
}

// SiaStats For parsing siastats data
type SiaStats struct {
	ID                 int    `json:"ID"`
	FinalScore         int    `json:"FinalScore"`
	ContractPrice      int    `json:"ContractPrice"`
	StoragePrice       int    `json:"StoragePrice"`
	UploadPrice        int    `json:"UploadPrice"`
	DownloadPrice      int    `json:"DownloadPrice"`
	Pubkey             string `json:"Pubkey"`
	AcceptingContracts bool   `json:"AcceptingContracts"`
}

// Config For the config file
type Config struct {
	Storage     float32 `json:"Storage"`
	Download    float32 `json:"Download"`
	Upload      float32 `json:"Upload"`
	TimeFrame   float32 `json:"TimeFrame"`
	RenewWindow float32 `json:"RenewWindow"`
	Redundancy  int     `json:"Redundancy"`
	MuseAddr    string  `json:"MuseAddr"`
	WalrusAddr  string  `json:"WalrusAddr"`
	ShardAddr   string  `json:"ShardAddr"`
	Seed        string  `json:"seed"`
	HostSet     string  `json: HostSet`
}

// HostInfo for the /scan enpoint of us
type HostInfo struct {
	AcceptingContracts     bool   `json:"acceptingContracts"`
	MaxDownloadBatchSize   uint64 `json:"maxDownloadbatchSize"`
	MaxDuration            int    `json:"maxDuration"`
	MaxReviseBatchSize     uint64 `json:"maxReviseBatchSize"`
	NetAddress             string `json:"netAddress"`
	RemainingStorage       uint64 `json:"remainingStorage"`
	SectorSize             uint64 `json:"sectorSize"`
	TotalStorage           uint64 `json:"totalStorage"`
	UnlockHash             string `json:"unlockHash"`
	WindowSize             int    `json:"windowSize"`
	Collateral             string `json:"collateral"`
	MaxCollateral          string `json:"maxCollateral"`
	BaseRPCPrice           string `json:"baseRPCPrice"`
	ContractPrice          string `json:"contractPrice"`
	DownloadBandwidthPrice string `json:"downloadBandwidthPrice"`
	SectorAccessPrice      string `json:"sectorAccessPrice"`
	StoragePrice           string `json:"storagePrice"`
	UploadBandwidthPrice   string `json:"uploadBandwidthPrice"`
	RevisionNumber         uint64 `json:"revisionNumber"`
	Version                string `json:"version"`
}

// FormPost for posting renew and form things
type FormPost struct {
	ID          string           `json:"id"`
	HostKey     string           `json:"hostKey"`
	Funds       string           `json:"funds"`
	StartHeight int              `json:"startHeight"`
	Endheight   int              `json:"endHeight"`
	Settings    FormPostSettings `json:"settings"`
}

// FormPostSetting for the nested stuff in formpost
type FormPostSettings struct {
	UnlockHash             string `json:"unlockHash"`
	WindowSize             int    `json:"windowSize"`
	Collateral             string `json:"collateral"`
	MaxCollateral          string `json:"maxCollateral"`
	ContractPrice          string `json:"contractPrice"`
	DownloadBandwidthPrice string `json:"downloadBandwidthPrice"`
	StoragePrice           string `json:"storagePrice"`
	UploadBandwidthPrice   string `json:"uploadBandwidthPrice"`
}

// ContractStruct
type ContractStruct struct {
	HostKey     string `json:"hostKey"`
	ID          string `json:"id"`
	RenterKey   string `json:"renterKey"`
	HostAddress string `json:"hostAddress"`
	EndHeight   int    `json:"endHeight"`
}

// dir type for directories
type DirData struct{
    Files 	[]string
    SubDirs []string
}

// file type for each individual file
type Filez struct{
    UsaFile 		[]byte
		FileName 		string
		Size 				int64
		MimeHeader 	textproto.MIMEHeader
		HostIDs		  []string
}

// host data type for use in the repair function
type HostData struct{
	History 		[]bool
}
