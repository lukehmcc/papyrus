package main

// This package handles switching from struct -> byte -> struct again

import (
	"bytes"
	"encoding/gob"
	"log"
	"fmt"
)

// encodes the struct
func encodeToBytes(p interface{}) []byte {
	buf := bytes.Buffer{}
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(p)
	check(err, "encoding to byte failed hard")
	fmt.Println("uncompressed size (bytes): ", len(buf.Bytes()))
	return buf.Bytes()
}

// decodes byte to file struct
func decodeToFile(s []byte) Filez {
	p := Filez{}
	// if the passed entry is empty, return nothing
	if(s == nil){
		return p
	}
	dec := gob.NewDecoder(bytes.NewReader(s))
	err := dec.Decode(&p)
	if err != nil {
		log.Fatal(err)
	}
	return p
}

// decodes byte to dir struct
func decodeToDirData(s []byte) DirData {
	p := DirData{}
	// if the passed entry is empty, return nothing
	if(s == nil){
		return p
	}
	dec := gob.NewDecoder(bytes.NewReader(s))
	err := dec.Decode(&p)
	if err != nil {
		log.Fatal(err)
	}
	return p
}

// decodes byte to hostdata struct
func decodeToHostData(s []byte) HostData {
	p := HostData{}
	// if the passed entry is empty, return nothing
	if(s == nil){
		return p
	}
	dec := gob.NewDecoder(bytes.NewReader(s))
	err := dec.Decode(&p)
	if err != nil {
		log.Fatal(err)
	}
	return p
}
