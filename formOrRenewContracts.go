package main

import (
	"bytes"
	"encoding/json"
	"log"
	"io/ioutil"
	"math/big"
	"net/http"
	"errors"
	"strconv"
)

// This thing either forms or renews a contract
// based on passed params

func formOrRenewContracts(pubkey string, ID string, endpoint string) error {
	println("formOrRenew-------------------------------------")
	// Grabs the config and turns it to struct
	configJSON := grabConfig()
	var hostKeyJSON HostKey
	hostKeyJSON.HostKey = pubkey
	data, err := json.Marshal(hostKeyJSON)
	if err != nil {
		log.Println("[formOrRenewContract] marshalling string failed")
		return err
	}
	// scans pubkey by turning json to byte and passing it to the
	// fetchpost function then unmarshals it
	resp, err := http.Post(string(configJSON.MuseAddr)+"/scan", "application/json", bytes.NewBuffer([]byte(data)))
	if err != nil {
		println("[formOrRenewContract] request to " + string(configJSON.MuseAddr) + "/scan" + " failed")
		return err
	}
	responseData, err := ioutil.ReadAll(resp.Body)
	log.Println(string(responseData))
	var hostInfoJSON HostInfo
	err = json.Unmarshal(responseData, &hostInfoJSON)
	if err != nil {
		log.Println("[formOrRenewContract] unmarshalling host data failed")
		return err
	}
	// Find amount needed to be allocated to contract
	hasting := toBigInt("1000000000000000000000000") // how many hastings in a SC
	BTB := 1000000000000
	// Now get the values into big ints
	storagePrice := toBigInt(hostInfoJSON.StoragePrice)
	uploadPrice := toBigInt(hostInfoJSON.UploadBandwidthPrice)
	downloadPrice := toBigInt(hostInfoJSON.DownloadBandwidthPrice)
	contractPrice := toBigInt(hostInfoJSON.ContractPrice)
	storageAmount := big.NewInt(int64(configJSON.Storage * float32(configJSON.Redundancy) * float32(BTB)))
	uploadAmount := big.NewInt(int64(configJSON.Upload * float32(BTB)))
	downloadAmount := big.NewInt(int64(configJSON.Download * float32(BTB)))
	contractAllownace := big.NewInt(1)
	// Big ints are annoying and manual
	// Multiply the storage amount * 1,000,000,000,000B = GB * 4320 = month
	multiply := big.NewInt(int64(4320 * configJSON.TimeFrame))
	storagePrice = storagePrice.Mul(storagePrice, multiply)
	storagePrice = storagePrice.Mul(storagePrice, storageAmount)
	contractAllownace = contractAllownace.Add(contractAllownace, storagePrice)
	log.Println("storage price", storagePrice.Div(storagePrice, hasting))
	// Multiply upload by 1,000,000,000B = GB
	uploadPrice = uploadPrice.Mul(uploadPrice, uploadAmount)
	contractAllownace = contractAllownace.Add(contractAllownace, uploadPrice)
	log.Println("upload price", storagePrice.Div(storagePrice, hasting))
	// Multiply download by 1,000,000,000B = GB
	downloadPrice = downloadPrice.Mul(downloadPrice, downloadAmount)
	contractAllownace = contractAllownace.Add(contractAllownace, downloadPrice)
	log.Println("download price", downloadPrice.Div(downloadPrice, hasting))
	// This once stays the same
	contractAllownace = contractAllownace.Add(contractAllownace, contractPrice)
	log.Println("contract price", contractPrice.Div(contractPrice, hasting))
	// Now check balance and endpoint
	resp = fetchGet(configJSON.WalrusAddr + "/balance")
	responseData, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("[formOrRenewContract] reading balance failed")
		return err
	}
	balanceBig := toBigInt(string(responseData)[1:len(string(responseData))-2])
	if balanceBig.Cmp(contractAllownace) == 0 {
		return err
	}
	// With that all complete, commence building the json
	var formContract FormPost
	var formContractSettings FormPostSettings
	// First check what endpoint
	if endpoint == "form" {
		formContract.HostKey = pubkey
	} else if endpoint == "renew" {
		formContract.ID = ID
	} else {
		return errors.New("[formOrRenewContract] enpoint not properly provided")
	}
	// Grab the current height
	resp = fetchGet(configJSON.ShardAddr + "/height")
	height, err := ioutil.ReadAll(resp.Body)
	heightInt, err := strconv.Atoi(string(height))
	if err != nil {
		log.Println("[formOrRenewContract] getting height failed")
		return err
	}
	// Then fill out the rest of the thingies
	// Okay so the reason I'm dividing the per-host allowance by the number of hosts / 2 is
	// because if you're storing 1TB of data, you aren't storing that much on every host so it
	// makes no sense to do that on every host. So here we set the alloance to what is
	// required to function. The health script recursively checks every contract to make sure it's
	// topped off, so this shouldn't be an issue and should reduce costs on formatin drastically
	var redun int
	if(configJSON.Redundancy == 0){
		redun = 3
	}else{
		redun = configJSON.Redundancy
	}
	divider := big.NewInt(int64(getNumHosts() / (redun / 2)))
	formContract.Funds = contractAllownace.Div(contractAllownace, divider).String()
	formContract.StartHeight = heightInt
	formContract.Endheight = heightInt + int(configJSON.TimeFrame*4320)
	// Then set the settings
	formContractSettings.UnlockHash = hostInfoJSON.UnlockHash
	formContractSettings.WindowSize = int(configJSON.RenewWindow * 4320)
	// this reduced collateral in an attempt to fix issues that arise from hosts changing prices on the fly
	formContractSettings.Collateral = hostInfoJSON.Collateral
	formContractSettings.MaxCollateral = hostInfoJSON.MaxCollateral
	formContractSettings.ContractPrice = hostInfoJSON.ContractPrice
	formContractSettings.DownloadBandwidthPrice = hostInfoJSON.DownloadBandwidthPrice
	formContractSettings.StoragePrice = hostInfoJSON.StoragePrice
	formContractSettings.UploadBandwidthPrice = hostInfoJSON.UploadBandwidthPrice
	// then apply it to the main struct
	formContract.Settings = formContractSettings
	// Now marshal that thing!
	formContractJSON, err := json.Marshal(formContract)
	if err != nil {
		log.Println("[formOrRenewContract] marshalling json failed")
		return err
	}
	log.Println(string(formContractJSON))
	log.Println(contractAllownace.Div(contractAllownace, hasting))
	// Finally make post request
	url := string(configJSON.MuseAddr) + "/renew"
	isform := false
	if endpoint == "form" {
		url = string(configJSON.MuseAddr) + "/form"
		isform = true
	}
	resp, err = http.Post(url, "application/json", bytes.NewBuffer([]byte(formContractJSON)))
	if err != nil {
		log.Println("[formOrRenewContract] request to " + url + " failed")
		return err
	}
	responseData, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("[formOrRenewContract] reding response from renew/form failed")
		return err
	}
	log.Println(string(responseData))
	log.Println("status code from contract form/renewer", resp.StatusCode)
	if resp.StatusCode == 500 && ID != "" {
		return errors.New("[formOrRenewContract] failed to sucsessfully make talks with the host")
	}
	// unmarshal that data
	var contractStructJSON ContractStruct
	err = json.Unmarshal(responseData, &contractStructJSON)
	if err != nil {
		log.Println("[formOrRenewContract] unmarshalling host data failed")
		return err
	}
	// if a contract sucseeds then it'll reach this point and create a badger entry if it is a form
	if(isform){
		// create a badger entry for the host
		var empty HostData
		empty.History = []bool{true}
		emptyBytes := encodeToBytes(empty)
		err := badgerSET(contractStructJSON.ID, emptyBytes, DBLoc)
		check(err, "[formOrRenew]creating entry for host failed")
	}
	log.Println("[formOrRenewContract] renew/form completed")
	return nil
}
