package main

import (
  "log"
  "fmt"
  "math"
  "net/http"
  "io/ioutil"
  "encoding/json"
  "bytes"
)

// This script checks over the hosts on an ongoing basis and

func hostCheckup() {
  // get current set of contracts
  // So grab the configs and the current contract set
	configJSON := grabConfig()
	resp, err := http.Get(configJSON.MuseAddr + "/contracts")
	if err != nil {
    log.Println("[hostCheckup] Grabbing contracts failed")
		return
	}
	responseData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
    log.Println("[hostCheckup] Reading the contract response failed")
		log.Println(err)
		return
	}
	// Then toss them into a json object
	var contracts []ContractStruct
	err = json.Unmarshal([]byte(responseData), &contracts)
	if err != nil {
		log.Println(err)
    log.Println("[hostCheckup] unmarshalling response data failed")
		return
	}
  // Now check up on all of these guys to make sure they're online, and if they are add to their score
  for i, contract := range contracts {
    // first get the host data
    hostDataByte, err := badgerGET(contract.ID, DBLoc)
    check(err, "decoding the host data failed")
    hostData := decodeToHostData(hostDataByte)
    // then scan the host to make sure it's online
    var hostKeyJSON HostKey
  	hostKeyJSON.HostKey = contract.HostKey
  	data, err := json.Marshal(hostKeyJSON)
  	if err != nil {
  		log.Println(err)
      log.Println("Marshalling the host key failed")
      continue
  	}
    resp, err := http.Post(string(configJSON.MuseAddr)+"/scan", "application/json", bytes.NewBuffer([]byte(data)))
  	if(resp.StatusCode != 200) {
      // If the host isn't online then add that to the list
      hostData.History = append(hostData.History, false)
      hostDataByte = encodeToBytes(hostData)
      err = badgerSET(contract.ID, hostDataByte, DBLoc)
      check(err, "setting to badger failed when trying to update host uptime")
  	}else{
      // if the host does respond then add that
      hostData.History = append(hostData.History, true)
      hostDataByte = encodeToBytes(hostData)
      err = badgerSET(contract.ID, hostDataByte, DBLoc)
      check(err, "setting to badger failed when trying to update host uptime")
    }
    _ = i
  }
  // Then print the current health of all of the contracts visually
  fmt.Println("________________________________________________________________")
  for i, contract := range contracts {
    // first get the host data
    hostDataByte, err := badgerGET(contract.ID, DBLoc)
    check(err, "decoding the host data failed")
    hostData := decodeToHostData(hostDataByte)
    up := 0
    down := 0
    for j, aBoolean := range hostData.History {
      if(aBoolean == true){
        up++
      }else{
        down++
      }
      _ = j
    }
    // now that info is gathered, print the uptime of each host
    if(down == 0){ // don't divide by 0 you idiot
      fmt.Println("| " + contract.ID + ": 100% uptime |")
    }
    uptime := fmt.Sprintf("%f", math.Round((1-float64(down/up))*100))
    fmt.Println("| " + contract.ID + ": " + uptime + "% uptime |")
    // if a host has more than 10 failed events and less than 80% uptime, they're getting culled
    if((1-float64(down/up)) > .8 && down > 10){
      resp, err := http.Post((configJSON.MuseAddr + "/delete/" + contract.ID), "application/json", bytes.NewBuffer([]byte("")))
			if(err != nil || resp.StatusCode == 500){
				print("[hostCheckup] request to delete endpoint failed")
			}
    }
    _ = i
  }
  fmt.Println("________________________________________________________________")
}
