package main

// This is the piece of the code that grabs all of the hosts,
// sorts them by price, then writes their IDs and some other
// data to a file(this data being contract price host pubkey)

// Do the imports
import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	// "log"
	"sort"
	"net/http"
	"bytes"
)

// And now let the magic flow
func pullHosts(numHosts int) (string, error) {
	url := "https://siastats.info:3510/hosts-api/allhosts"

	// Grabs the current host stats
	//defines post thingy
	values := map[string]string{"network": "sia", "list": "active"}
	jsonValue, err := json.Marshal(values)
	if err != nil {
		return "[pull host] siastats json marshal failed", err
	}
	// fetch
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		fmt.Println("request to " + url + " failed")
		return "pulling data from siastats failed", err
	}
	// turn to bite
	responseData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		check(err, "reading the response failed")
	}

	// Now parse that into the struct(that's stored in structs.go)
	var jsonArray []SiaStats
	err = json.Unmarshal([]byte(responseData), &jsonArray)
	if err != nil {
		check(err, "unmarshalling to siastats struct failed")
	}
	// Define key list
	hostList := [][]int{}
	// So make a 2d array of score and index and combo price
	for i := 0; i < len(jsonArray); i++ {
		if jsonArray[i].AcceptingContracts == true {
			tempArray := []int{}
			tempArray = append(tempArray, jsonArray[i].FinalScore)
			tempArray = append(tempArray, i)
			tempArray = append(tempArray, jsonArray[i].ContractPrice*5+jsonArray[i].StoragePrice*3+jsonArray[i].DownloadPrice*2+jsonArray[i].UploadPrice)
			hostList = append(hostList, tempArray)
		}
	}
	// Then cull that list on size
	sort.SliceStable(hostList, func(i, j int) bool {
		return hostList[i][0] < hostList[j][0]
	})
	hostListBestPerf := hostList[len(hostList)-(numHosts*2):]
	// Finally sort by price, and cull top whatever by that
	sort.SliceStable(hostListBestPerf, func(i, j int) bool {
		return hostListBestPerf[i][0] < hostListBestPerf[j][0]
	})
	hostListBestPrice := hostListBestPerf[len(hostListBestPerf)-numHosts:]
	println(len(hostListBestPrice))
	// Make that into a single json file
	culledList := make([]SiaStats, 0)
	for i := 0; i < len(hostListBestPrice); i++ {
		culledList = append(culledList, jsonArray[hostListBestPrice[i][1]])
	}
	culledListJSON, err := json.Marshal(culledList)
	if err != nil {
		fmt.Println(err)
		return "[pull host] marshalling json failed", err
	}
	// Write that to disk
	err = ioutil.WriteFile(workingPath()+"/hostList.json", culledListJSON, 0644)
	if err != nil {
		fmt.Println(err)
		return "[pull host] io write failed", err
	}
	// Returns
	return "host sort sucsessful & written to disk", nil
}
