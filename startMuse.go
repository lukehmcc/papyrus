package main

import (
	"log"
	"net/http"
	"sync"

	"lukechampine.com/muse"
	"lukechampine.com/walrus"
)

// This starts muse pretty easily

func startMuse(wg *sync.WaitGroup) {
	defer wg.Done()
	// Grab config
	configJSON := grabConfig()
	// Initialize walrus
	wc := walrus.NewClient(configJSON.WalrusAddr)
	// Now start up muse and check for errors
	museServer, err := muse.NewServer(workingPath(), wc.ProtoWallet(grabDatSeed()), wc.ProtoTransactionPool(), configJSON.ShardAddr)
	if err != nil {
		log.Fatalln("Could not initialize server:", err)
	}
	log.Printf("Listening on %v...", ":9580")
	log.Fatal(http.ListenAndServe(":9580", museServer))
}
