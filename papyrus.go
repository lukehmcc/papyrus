package main

import (
	"fmt"
	"io/ioutil"
	"sync"
)

var DBLoc string // global var for the db locaiton, probably will be changed

// This is the main script in go that
// Controlls everything
func main() {
	fmt.Println("Starting...")
	// First test if the main config exists
	_, err := ioutil.ReadFile(workingPath() + "/config.json")
	if err != nil {
		problem := initConfig()
		fmt.Println(problem)
	}
	// Define wait group
	var wg sync.WaitGroup
	wg.Add(1)
	// Start muse
	go startMuse(&wg)
	// Start api
	go startAPI(&wg)
	// Call health scripts and pass wg, though don't iterate
	go healthUpkeep(&wg)
	// define the db locaiton
	DBLoc = workingPath() + "/Badger"
	// Wait until the api tells the script to shut down
	wg.Wait()
}
