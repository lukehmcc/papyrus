package main

import (
	"fmt"
	"log"
	"net/http"
	"io/ioutil"
	"os"
	"sync"
	// "net/textproto"
	"strconv"
	// "math/big"

	// "github.com/pkg/errors"
	// "golang.org/x/crypto/sshbig.NewInt(responseData)/terminal"
	// "lukechampine.com/us/renter"
	// "lukechampine.com/us/renter/renterutil"
	// "lukechampine.com/us/renterhost"
)

// WorkerGroupHandler This object passes around the worker group
type WorkerGroupHandler struct {
	WG *sync.WaitGroup
}

// Main func that defines other functions
func startAPI(wg *sync.WaitGroup) {
	// Home
	myWorkerGroupHandler := &WorkerGroupHandler{WG: wg}
	http.HandleFunc("/", myWorkerGroupHandler.handle)
	log.Fatal(http.ListenAndServe(":9500", nil))
}

// handle endpoint
func (wgh *WorkerGroupHandler) handle(w http.ResponseWriter, r *http.Request) {
	userPath := r.URL.Path // makes it easier to type lol
	if(userPath == "/"){
		serveOnline(w, r)
	}else if(len(userPath) == 5 && userPath[:5] == "/stop"){
		wgh.stop(w, r)
	}else if(userPath[:5] == "/stat"){
		serveStat(w, r, userPath[5:])
	}else if(userPath[:7] == "/upload"){
		serveUploadFile(w, r, userPath[7:])
	}else if(len(userPath) == 8 && userPath[:8] == "/balance"){
		serveBalance(w, r)
	}else if(userPath[:9] == "/download"){
		serveDownloadFile(w, r, userPath[9:])
	}else if(userPath[:17] == "/refreshcontracts"){
		contractRenewDelete()
		formContracts()
	}else{
		fmt.Fprintf(w, userPath + " is not an endpoint")
	}
}
func serveOnline(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Online endpoint hit")
	fmt.Fprintf(w, "We are all good!(well at least the API is online in some regard)")
}

// upload file
func serveUploadFile(w http.ResponseWriter, r *http.Request, virtualPath string) {
	// make sure that virtualpath isn't empty, if it is it'll cause problems
	if(virtualPath == ""){
		virtualPath = "/"
	}
  fmt.Println("File Upload Endpoint Hit")
  // Parse our multipart form, 10 << 20 specifies a maximum
  // upload of 10 MB files.
  r.ParseMultipartForm(10 << 20)
  // FormFile returns the first file for the given key `file`
  // it also returns the FileHeader so we can get the Filename,
  // the Header and the size of the file
  file, handler, err := r.FormFile("file")
	// The reason to not use check here is because a failed upload should not
	// result in the program quitting fatally
  if err != nil {
    fmt.Println("Error Retrieving the File")
    fmt.Println(err)
    return
  }
  defer file.Close()
  fmt.Printf("Uploaded File: %+v\n", handler.Filename)
  fmt.Printf("File Size: %+v\n", handler.Size)
  fmt.Printf("MIME Header: %+v\n", handler.Header)
	// Create a temporary file within our temp-images directory that follows
	// a particular naming pattern
	tempPath := workingPath()
	tempPath += "/tempUploads"
	tempFile, err := ioutil.TempFile(tempPath, "upload-737")
	check(err, "creating tempfile failed")
	defer os.Remove(tempFile.Name())
  // read all of the contents of our uploaded file into a byte array
  fileBytes, err := ioutil.ReadAll(file)
  check(err, "reading tempfile failed")
  // write this byte array to our temporary file
  tempFile.Write(fileBytes)
	// now upload that meta file
	// minshards is a hardcoded value for now; pls change this later
	err = newUploadFile(tempFile.Name(), 10, makeHostSet())
	check(err, "uploading meta file didn't work:")
	// Now that the file has been uploaded we gotta add the .usa to badger
	fileBytes, err = ioutil.ReadFile(tempFile.Name() +  ".usa")
  check(err, "reading .usa tempfile failed")
	// make a filez struct
	var fileData Filez
	fileData.UsaFile = fileBytes
	fileData.FileName = handler.Filename
	fileData.Size = handler.Size
	fileData.MimeHeader = handler.Header
	// grab current host set and add it to the file data
	configJSON := grabConfig()
	fileData.HostIDs = listCurrentContractIDs(configJSON.MuseAddr, "default")
	// Turn that to byte
	fileDataByte := encodeToBytes(fileData)
	// Add to the virtual file
	log.Println("setting virtual file,", fileName(tempFile.Name()))
	check(badgerSET(fileName(tempFile.Name()), fileDataByte, workingPath()+"/Badger"), "setting file to badger failed")
	// attempt to grab the folder data for the virtual dir and if it doesn't exist, make it
	if(badgerExistanceCheck(virtualPath) != true){
		var newDir DirData
		newDir.Files = append(newDir.Files, fileName(tempFile.Name()))
		dirDataByte := encodeToBytes(newDir)
		log.Println("Creating virtualPath dir:", virtualPath)
		check(badgerSET(virtualPath, dirDataByte, DBLoc), "setting dir to badger failed")
	}else{
		dirDataByte, err := badgerGET(virtualPath, DBLoc)
		check(err, "getting dir data failed hard when trying to create the folder that we're working with")
		dirData := decodeToDirData(dirDataByte)
		dirData.Files = append(dirData.Files, fileName(tempFile.Name()))
		dirDataByte = encodeToBytes(dirData)
		log.Println("Creating virtualPath dir:", virtualPath)
		check(badgerSET(virtualPath, dirDataByte, DBLoc), "setting dir to badger failed")
	}
	// also recursively check the parent directories to make sure it's navigable to
	var virtualPathCopyTop string
	var virtualPathCopyBottom string
	virtualPathCopyBottom += virtualPath
	virtualPathCopyTop += parentDir(virtualPath)
	keepChecking := true
	for(parentDir(virtualPathCopyTop) != virtualPathCopyTop && keepChecking){
		 // check if this parent dir exists, if it does, just grab it and append to it the subdirectory you just created
		 if(badgerExistanceCheck(virtualPathCopyTop) != true){
			 log.Println("creating parent directory:", virtualPathCopyTop)
			 var dirData DirData
			 dirData.SubDirs = append(dirData.SubDirs, virtualPathCopyBottom)
			 dirDataByte := encodeToBytes(dirData)
			 err = badgerSET(virtualPathCopyTop, dirDataByte, DBLoc)
			 check(err, "setting to badger failed")
		 // if this parent dir doesn't formContracts()exsit, create it and put the lower level dir into it
		 }else{
			 log.Println("appending to parent directory:", virtualPathCopyTop)
			 dirDataByte, err := badgerGET(virtualPathCopyTop, DBLoc)
			 check(err, "grabbing directory from badger failed")
			 dirData := decodeToDirData(dirDataByte)
			 dirData.SubDirs = append(dirData.SubDirs, virtualPathCopyBottom)
			 dirDataByte = encodeToBytes(dirData)
			 err = badgerSET(virtualPathCopyTop, dirDataByte, DBLoc)
			 check(err, "setting to badger failed")
			 keepChecking = false
		 }
		 virtualPathCopyTop = parentDir(virtualPathCopyTop)
		 virtualPathCopyBottom = parentDir(virtualPathCopyBottom)
	}

	// return that we have successfully uploaded our file!
  fmt.Fprintf(w, "Successfully Uploaded File\n")
}

func serveDownloadFile(w http.ResponseWriter, r *http.Request, virtualPath string) {
  fmt.Println("File Download Endpoint Hit")
	// check that the file both exists and the path being asked is a file
	if(badgerExistanceCheck(parentDir(virtualPath)) != true){
		fmt.Fprintf(w, parentDir(virtualPath) + " is not a valid parent directory")
		return
	}
	// now find the metafile location
	var fileData Filez
	dirDataByte, err := badgerGET(parentDir(virtualPath), DBLoc)
	check(err, "getting badger for download failed for directory: " + virtualPath)
	dirData := decodeToDirData(dirDataByte)
	for i, o := range dirData.Files{
		fileByte, err := badgerGET(o, DBLoc)
		check(err, "getting file failed to work in stat api endpoint")
		file := decodeToFile(fileByte)
		if(file.FileName == fileName(virtualPath)){
			fileData = file
		}
		_ = i
	}
	// make sure the file isn't null
	if(fileData.FileName == ""){
		fmt.Fprintf(w, virtualPath +" is not a valid file to download")
		return
	}
	// write that metafile to disk
	tempMeta, err := ioutil.TempFile(getMetaPath(), "prefix-*.usa")
  check(err, "Cannot create temporary file")
  // Remember to clean up the file afterwards
  defer os.Remove(tempMeta.Name())
  // Now write the .usa to the file
  _, err = tempMeta.Write(fileData.UsaFile)
  check(err, "Failed to write to temporary file")
	// now create the file location for the file to go to and download it
	tempFile, err := ioutil.TempFile(getTempPath(), fileData.FileName + "-*")
  check(err, "Cannot create temporary file")
  // Remember to clean up the file afterwards
  defer tempFile.Close()
	// download acutal file to the temp directory
	err = downloadmetafile(tempFile, makeHostSet(), tempMeta.Name())
	check(err, "Download failed for: " + virtualPath)
	// send that file to the user
	w.Header().Set("Content-Disposition", "attachment; filename="+strconv.Quote(fileData.FileName))
	w.Header().Set("Content-Type", mediaTypeOrDefault(fileData.MimeHeader))
	http.ServeFile(w, r, tempFile.Name())
}

func serveStat(w http.ResponseWriter, r *http.Request, virtualPath string){
	fmt.Println("Stat Endpoint Hit\n")
	// make sure that virtualpath isn't empty, if it is it'll cause problems
	if(virtualPath == ""){
		virtualPath = "/"
	}
	if(badgerExistanceCheck(virtualPath) != true){
		fmt.Fprintf(w, "The path " + virtualPath + " does not exist\n")
		return
	}
	// now check if it's a file or a dir
	if(virtualPath[len(virtualPath)-1:len(virtualPath)] == "/"){
		log.Println("statting dir")
		dirDataByte, err := badgerGET(virtualPath, DBLoc)
		check(err, "getting badger for stat failed")
		dirData := decodeToDirData(dirDataByte)
		fmt.Fprintf(w, "The directory in " + virtualPath + " are: \n")
		for i, o := range dirData.SubDirs{
			fmt.Fprintf(w, o + "\n")
			_ = i
		}
		fmt.Fprintf(w, "The files in " + virtualPath + " are: \n")
		for i, o := range dirData.Files{
			fileByte, err := badgerGET(o, DBLoc)
			check(err, "getting file failed to work in stat api endpoint")
			file := decodeToFile(fileByte)
			fmt.Fprintf(w, file.FileName + "\n")
			_ = i
		}
	}else{
		log.Println("statting file")
		fmt.Fprintf(w, "There is a file at " + virtualPath)
	}
}

// Stop endpoint
func (wgh *WorkerGroupHandler) stop(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "stopping!\n")
	fmt.Println("Stop endpoint hit")
	if f, ok := w.(http.Flusher); ok {
		f.Flush()
	}
	wgh.WG.Done()
}

// balance enpoint
func serveBalance(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Balance Endpoint Hit")
	resp, err := http.Get(grabConfig().WalrusAddr+"/balance")
	responseData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	amount := toBigInt(string(responseData)[1:len(string(responseData))-2])
	amountSC := amount.Div(amount, toBigInt("1000000000000000000000000"))
	fmt.Fprintf(w, "Balance: " + amountSC.String() + "SC\n")
}
