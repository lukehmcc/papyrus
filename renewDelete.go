package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

// here's the script that'll either form, keep, or delete
// the contracts depending on the current height and the
// contract period

func contractRenewDelete() string {
	// So grab the configs and the current contract set
	configJSON := grabConfig()
	resp, err := http.Get(configJSON.MuseAddr + "/contracts")
	if err != nil {
		return ("[renewDelete] call to muse/contracts failed")
	}
	responseData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return "[renewDelete] reading /contracts endpoint failed "
	}
	// Then toss them into a json object
	var contracts []ContractStruct
	err = json.Unmarshal([]byte(responseData), &contracts)
	if err != nil {
		fmt.Println(err)
		return "[renewDelete] unmarshalling /contracts endpoint failed "
	}
	// Now check the heights one by one
	for i := 0; i < len(contracts); i++ {
		// Grab block height while making sure to keep requesting until the actual
		// block height is returned without error
		fmt.Println("checking...\n", contracts[i])
		height := 0
		acc := 0
		for height == 0 {
			height = getHeight()
			acc++
			time.Sleep(1 * time.Second)
			if acc == 10 {
				return "[renewDelete] grabbing height failed specatcularly"
			}
		}
		if height > contracts[i].EndHeight {
			// If contract is older than expiration height, delete it
			fmt.Println("deleting: " + contracts[i].ID)
			var test ContractStruct
			test.ID = contracts[i].ID
			if err != nil {
				fmt.Println(err)
				return "[renewDelete] marshaling struct failed"
			}
			// posts to deletion and check for errors
			resp, err := http.Post((configJSON.MuseAddr + "/delete/" + contracts[i].ID), "application/json", bytes.NewBuffer([]byte("")))
			if err != nil {
				print("[renewDelete] request to delete endpoint failed")
			}
			respData, _ := ioutil.ReadAll(resp.Body)
			fmt.Println(respData)
		} else if height > (contracts[i].EndHeight - int(4320*configJSON.RenewWindow)) {
			// If contract is within renew window, renew it
			fmt.Println("Renewing " + contracts[i].ID)
			resp := formOrRenewContracts(contracts[i].HostKey, contracts[i].ID, "renew")
			fmt.Println(resp)
		} else {
			// If contract is fine, do nothing
		}
	}
	return "[renewDelete] Contracts sucsessfully renewed/deleted"
}
