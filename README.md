# papyrus

A simplified and modern UI to the Sia network build on <a src="https://github.com/lukechampine/us">`us`<a>

<b>TO DO(in a particular order)<b>
- ~~Complete renew script~~
- ~~Complete init & main daemon~~
- ~~Rewrite the thing in go~~
- Add API
    - ~~/online~~
    - ~~/stop~~
    - /contracts
    - /wallet
        - /show (this shows other info about the wallet like addresses)
        - /send
        - ~~/balance~~
    - /allowance
        - /show
        - /update
    - /renter
        - /upload
          - ~~single file~~
          - directory
        - /download
          - ~~single file~~
          - directory
        - /delete
          - single file
          - directory
- ~~Complete a rudementary contract repair script~~
- Check seeds to make sure they're the right length
- Add seed generator
- Add send/recieve SC fucntionality to main script
- Put limiters on renew as to cull excess spending
- Make it so it doesn't rely on Luke's servers(so fully local mode as an option/fallback)
- Add cli that talks to all those endpoints
- Add option to make your own contract list
- Build a UI
- SkyDB integration
- Transak integration
- FUSE support for CLI
