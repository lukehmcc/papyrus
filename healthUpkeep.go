package main

import (
	"sync"
	"time"
)

// this is the main health check script that makes
// sure contracts and files stay gucci
func healthUpkeep(wg *sync.WaitGroup) {
	for 1 > 0 {
		defer wg.Done()
		// deletes old, renews what it can
		contractRenewDelete()
		// check up on hosts and update stats stored in badger
		hostCheckup()
		// Now find the amount of contracts(in default) and form
		formContracts()
		// Migrates files if need be
		recursiveMigrateFiles()
		time.Sleep(5 * time.Minute)
	}
}
