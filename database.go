package main

// just database calls

import (
    badger "github.com/dgraph-io/badger/v3"
    "log"
)

// does the set thiny
func badgerSET(virtualLoc string, data []byte, DBLoc string) error {
  // initialize
  opt := badger.DefaultOptions(DBLoc)
  opt.Logger = nil
  db, err := badger.Open(opt)
  if err != nil {
    log.Fatalf("unable to open db: %s", err)
    return err
  }
  defer db.Close()
  // now set an entry
  err = db.Update(func(txn *badger.Txn) error {
    return txn.Set([]byte(virtualLoc), data)
  })
  if err != nil {
    log.Fatalf("unable to set key: %s", err)
    return err
  }
  return nil
}

// gets from the db, if nothing, return null
func badgerGET(virtualLoc string, DBLoc string)([]byte, error) {
  // initialize
  opt := badger.DefaultOptions(DBLoc)
  opt.Logger = nil
  db, err := badger.Open(opt)
  if err != nil{
    log.Fatalf("unable to open db: %s", err)
    return []byte(""), err
  }
  defer db.Close()
  var realValCopy []byte
  // now get an entry
  err = db.View(func(txn *badger.Txn) error {
    item, err := txn.Get([]byte(virtualLoc))
    if err != nil{
      log.Fatalf("unable get key from db: %s", err)
      return err
    }
    // Then yeet it to the main process
    realValCopy, err = item.ValueCopy(nil)
    if err != nil{
        log.Fatalf("unable to copy value: %s", err)
        return err
    }
    return nil
  })
  if err != nil{
    log.Fatalf("unable to get from db: %s", err)
    return []byte(""), err
  }
  // and print it
  return realValCopy, nil
}

// deletes from the db, if nothing, return null
func badgerDELETE(virtualLoc string, DBLoc string) error {
  // initialize
  opt := badger.DefaultOptions(DBLoc)
  opt.Logger = nil
  db, err := badger.Open(opt)
  if err != nil{
    log.Fatalf("unable to open db: %s", err)
    return err
  }
  defer db.Close()
  // now get an entry
  err = db.View(func(txn *badger.Txn) error {
    err := txn.Delete([]byte(virtualLoc))
    if err != nil{
      log.Fatalf("unable remove key from db: %s", err)
      return err
    }
    return nil
  })
  if err != nil{
    log.Fatalf("unable to remove from db: %s", err)
    return err
  }
  return nil
}

func badgerExistanceCheck(virtualLoc string) bool{
  // initialize
  opt := badger.DefaultOptions(DBLoc)
  opt.Logger = nil
  db, err := badger.Open(opt)
  if err != nil{
    log.Fatalf("unable to open db: %s", err)
  }
  defer db.Close()
  // now attempt to get the location DBLoc
  err = db.View(func(txn *badger.Txn) error {
    _, err := txn.Get([]byte(virtualLoc))
    if(err != nil){
      return err
    }
    return nil
  })
  // if the directory doesn't exist, return false
  if err != nil {
    log.Println("Virtual location is:", virtualLoc, "and it doesn't exist")
    return false
  }else{
    log.Println("Virtual location is:", virtualLoc, "and it does exist")
    return true
  }
}
