module papyrustore.com/m

go 1.15

require (
	filippo.io/edwards25519 v1.0.0-beta.3 // indirect
	github.com/coreos/go-etcd v2.0.0+incompatible // indirect
	github.com/cpuguy83/go-md2man v1.0.10 // indirect
	github.com/dgraph-io/badger/v3 v3.2011.1 // indirect
	github.com/hanwen/go-fuse/v2 v2.1.0 // indirect
	github.com/inconshreveable/go-update v0.0.0-20160112193335-8152e7eb6ccf // indirect
	github.com/julienschmidt/httprouter v1.3.0 // indirect
	github.com/karalabe/hid v1.0.0 // indirect
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0 // indirect
	github.com/karrick/godirwalk v1.10.12 // indirect
	github.com/klauspost/cpuid v1.3.1 // indirect
	github.com/klauspost/cpuid/v2 v2.0.3 // indirect
	github.com/klauspost/reedsolomon v1.9.11 // indirect
	github.com/pkg/errors v0.9.1
	github.com/ugorji/go/codec v0.0.0-20181204163529-d75b2dcb6bc8 // indirect
	gitlab.com/NebulousLabs/Sia v1.5.5
	gitlab.com/NebulousLabs/log v0.0.0-20201012072136-659df99ce4a1 // indirect
	gitlab.com/NebulousLabs/siamux v0.0.0-20210210103854-9bdf3025036b // indirect
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777 // indirect
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
	golang.org/x/text v0.3.5 // indirect
	lukechampine.com/flagg v1.1.1
	lukechampine.com/frand v1.3.0
	lukechampine.com/muse v0.6.6
	lukechampine.com/shard v0.3.7 // indirect
	lukechampine.com/sialedger v1.0.0
	lukechampine.com/us v0.19.1
	lukechampine.com/walrus v0.10.5
)
