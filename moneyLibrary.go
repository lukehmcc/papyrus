package main

// This is pretty much just me stealing code from https://github.com/lukechampine/walrus-cli
// and re-writing it so it'll funciton without the bash inputs

// #include <string.h>
import (
	"C"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math/big"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"

	"gitlab.com/NebulousLabs/Sia/types"
	"lukechampine.com/flagg"
	"lukechampine.com/us/wallet"
)
import (
	"bufio"
	"os/user"

	"gitlab.com/NebulousLabs/Sia/crypto"

	"lukechampine.com/sialedger"
	"lukechampine.com/walrus"
)

// ------------------------ Luke's functions from walrus-cli ---------------------------------
var (
	// to be supplied at build time
	githash   = "?"
	builddate = "?"
)

func plural(n int) string {
	if n == 1 {
		return ""
	}
	return "s"
}

func currencyUnits(c types.Currency) string {
	r := new(big.Rat).SetFrac(c.Big(), types.SiacoinPrecision.Big())
	sc := strings.TrimRight(r.FloatString(30), "0")
	return strings.TrimSuffix(sc, ".") + " SC"
}

func parseCurrency(s string) types.Currency {
	r, ok := new(big.Rat).SetString(strings.TrimSpace(s))
	if !ok {
		_, err := strconv.ParseFloat(strings.TrimSpace(s), 64)
		check(err, "Invalid currency value")
	}
	return types.SiacoinPrecision.MulRat(r)
}

func readTxn(filename string) types.Transaction {
	js, err := ioutil.ReadFile(filename)
	check(err, "Could not read transaction file")
	var txn types.Transaction
	err = json.Unmarshal(js, &txn)
	check(err, "Could not parse transaction file")
	return txn
}

func writeTxn(filename string, txn types.Transaction) {
	js, _ := json.MarshalIndent(txn, "", "  ")
	js = append(js, '\n')
	err := ioutil.WriteFile(filename, js, 0666)
	check(err, "Could not write transaction to disk")
}

func getDonationAddr(narwalAddr string) (types.UnlockHash, bool) {
	u, err := url.Parse(narwalAddr)
	if err != nil {
		return types.UnlockHash{}, false
	}
	path := strings.Split(u.Path, "/")
	if len(path) < 2 || path[len(path)-2] != "wallet" {
		return types.UnlockHash{}, false
	}
	path = append(path[:len(path)-2], "donations")
	u.Path = strings.Join(path, "/")
	resp, err := http.Get(u.String())
	if err != nil {
		return types.UnlockHash{}, false
	}
	defer resp.Body.Close()
	defer ioutil.ReadAll(resp.Body)
	var addr types.UnlockHash
	err = json.NewDecoder(resp.Body).Decode(&addr)
	return addr, err == nil
}

var getNanoS = func() func() *sialedger.NanoS {
	var nanos *sialedger.NanoS
	return func() *sialedger.NanoS {
		if nanos == nil {
			var err error
			nanos, err = sialedger.OpenNanoS()
			check(err, "Could not connect to Nano S")
		}
		return nanos
	}
}()

var getSeed = func() func() wallet.Seed {
	var seed wallet.Seed
	return func() wallet.Seed {
		if seed == (wallet.Seed{}) {
			phrase := os.Getenv("WALRUS_SEED")
			if phrase != "" {
				fmt.Println("Using WALRUS_SEED environment variable")
			} else {
				fmt.Print("Seed: ")
				pw := grabSeed()
				fmt.Println()
				phrase = string(pw)
			}
			var err error
			seed, err = wallet.SeedFromPhrase(phrase)
			check(err, "Invalid seed")
		}
		return seed
	}
}()

func getChangeFlow(c *walrus.Client, ledger bool) types.UnlockHash {
	var pubkey types.SiaPublicKey
	fmt.Println("This transaction requires a 'change output' that will send excess coins back to your wallet.")
	index, err := c.SeedIndex()
	check(err, "Could not get next seed index")
	if ledger {
		fmt.Println("Please verify and accept the prompt on your device to generate a change address.")
		fmt.Println("(You may use the --change flag to specify a change address in advance.)")
		_, pubkey, err = getNanoS().GetAddress(uint32(index), false)
		check(err, "Could not generate address")
		fmt.Println("Compare the address displayed on your device to the address below:")
		fmt.Println("    " + wallet.StandardAddress(pubkey).String())
	} else {
		pubkey = getSeed().PublicKey(index)
		fmt.Println("Derived address from seed:")
		fmt.Println("    " + wallet.StandardAddress(pubkey).String())
	}
	err = c.AddAddress(wallet.SeedAddressInfo{
		UnlockConditions: wallet.StandardUnlockConditions(pubkey),
		KeyIndex:         index,
	})
	check(err, "Could not add address to wallet")
	fmt.Println("Change address added successfully.")
	fmt.Println()
	return wallet.StandardAddress(pubkey)
}

func broadcastFlow(c *walrus.Client, txn types.Transaction) error {
	err := c.Broadcast([]types.Transaction{txn})
	if err != nil {
		return err
	}
	fmt.Println("Transaction broadcast successfully.")
	fmt.Println("Transaction ID:", txn.ID())
	return nil
}

func signFlowCold(c *walrus.Client, txn *types.Transaction) error {
	nanos := getNanoS()
	addrs, err := c.Addresses()
	check(err, "Could not get addresses")
	addrMap := make(map[types.UnlockHash]struct{})
	for _, addr := range addrs {
		addrMap[addr] = struct{}{}
	}
	sigMap := make(map[int]uint64)
	for _, in := range txn.SiacoinInputs {
		addr := in.UnlockConditions.UnlockHash()
		if _, ok := addrMap[addr]; ok {
			// get key index
			info, err := c.AddressInfo(addr)
			check(err, "Could not get address info")
			// add signature entry
			sig := wallet.StandardTransactionSignature(crypto.Hash(in.ParentID))
			txn.TransactionSignatures = append(txn.TransactionSignatures, sig)
			sigMap[len(txn.TransactionSignatures)-1] = info.KeyIndex
			continue
		}
	}
	if len(sigMap) == 0 {
		fmt.Println("Nothing to sign: transaction does not spend any outputs recognized by this wallet")
		return nil
	}
	// request signatures from device
	fmt.Println("Please verify the transaction details on your device. You should see:")
	for _, sco := range txn.SiacoinOutputs {
		fmt.Println("   ", sco.UnlockHash, "receiving", currencyUnits(sco.Value))
	}
	for _, fee := range txn.MinerFees {
		fmt.Println("    A miner fee of", currencyUnits(fee))
	}
	if len(sigMap) > 1 {
		fmt.Printf("Each signature must be completed separately, so you will be prompted %v times.\n", len(sigMap))
	}
	for sigIndex, keyIndex := range sigMap {
		fmt.Printf("Waiting for signature for input %v, key %v...", sigIndex, keyIndex)
		sig, err := nanos.SignTxn(*txn, uint16(sigIndex), uint32(keyIndex))
		check(err, "Could not get signature")
		txn.TransactionSignatures[sigIndex].Signature = sig[:]
		fmt.Println("Done")
	}
	return nil
}

func signFlowHot(c *walrus.Client, txn *types.Transaction) error {
	seed := getSeed()
	fmt.Println("Please verify the transaction details:")
	for _, sco := range txn.SiacoinOutputs {
		fmt.Println("   ", sco.UnlockHash, "receiving", currencyUnits(sco.Value))
	}
	for _, fee := range txn.MinerFees {
		fmt.Println("    A miner fee of", currencyUnits(fee))
	}

	old := len(txn.TransactionSignatures)
	err := c.ProtoWallet(seed).SignTransaction(txn, nil)
	if err != nil {
		return err
	} else if old == len(txn.TransactionSignatures) {
		fmt.Println("Nothing to sign: transaction does not spend any outputs recognized by this wallet")
		return nil
	}
	return nil
}

// --------------------------- "my" functions(luke did most of this already) --------------------------------------

// Grabs the user's seed
func grabSeed() string {
	// Grab user's home dir and do some reading
	usr, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}
	dir := usr.HomeDir + "/.config/papyrus/seed.json"
	importedPre := importText(dir)
	// Declared an empty map interface
	var result map[string]interface{}
	// Unmarshal or Decode the JSON to the interface.
	json.Unmarshal([]byte(importedPre[0]), &result)
	// Reading each value by its key
	return result["seed"].(string)
}

// Import text as list
func importText(fileName string) []string {
	// open text file
	f, err := os.Open(fileName)
	//checks for error
	if err != nil {
		log.Fatal(err)
	}
	//tells it to close the file once program dies
	defer f.Close()
	// Defines list to be appended to
	listOfThings := []string{}
	// Imports file scanner
	scanner := bufio.NewScanner(f)
	//Goes line by line and scans
	for scanner.Scan() {
		temp := scanner.Text()
		listOfThings = append(listOfThings, temp)
	}
	// checks for errors
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return listOfThings
}

// The send money function!
//export sendMoney
func sendMoney(api *C.char, wallToSendTo *C.char, amount *C.char) *C.char {
	// Turn arguments into go strings
	apiAddr := C.GoString(api)
	sendToWallet := C.GoString(wallToSendTo)
	amountToSend := C.GoString(amount)
	// Define stuff to make function work
	rootCmd := flagg.Root
	ledger := rootCmd.Bool("ledger", false, "use a Ledger Nano S instead of a seed")
	c := walrus.NewClient(apiAddr)
	var changeAddrStr string
	var sign, broadcast bool
	broadcast = true
	sign = true
	// Now checks the wallet address to be valid
	outputs := make([]types.SiacoinOutput, 1)                                // Makes an output list length 1
	var recipSum types.Currency                                              // defines currency
	err := outputs[0].UnlockHash.LoadString(strings.TrimSpace(sendToWallet)) // Checks validity of the wallet in question
	check(err, "Invalid destination address")                                // Checks for errors
	outputs[0].Value = parseCurrency(amountToSend)                           // Adds the amount of currency to the output
	recipSum = recipSum.Add(outputs[0].Value)                                // Appends this thing with the amount to be send

	// if using a narwal server, compute donation
	var donation types.Currency
	donationAddr, ok := getDonationAddr(apiAddr)
	if ok {
		// donation is max(1%, 10SC)
		donation = recipSum.MulRat(big.NewRat(1, 100))
		if tenSC := types.SiacoinPrecision.Mul64(10); donation.Cmp(tenSC) < 0 {
			donation = tenSC
		}
	}

	// fund transaction
	utxos, err := c.UnspentOutputs(true)
	check(err, "Could not get utxos")
	inputs := make([]wallet.ValuedInput, len(utxos))
	for i, o := range utxos {
		info, err := c.AddressInfo(o.UnlockHash)
		check(err, "Could not get address info")
		inputs[i] = wallet.ValuedInput{
			SiacoinInput: types.SiacoinInput{
				ParentID:         o.ID,
				UnlockConditions: info.UnlockConditions,
			},
			Value: o.Value,
		}
	}
	feePerByte, err := c.RecommendedFee()
	check(err, "Could not get recommended transaction fee")
	used, fee, change, ok := wallet.FundTransaction(recipSum.Add(donation), feePerByte, inputs)
	if !ok {
		// couldn't afford transaction with donation; try funding without
		// donation and "donate the change" instead
		used, fee, change, ok = wallet.FundTransaction(recipSum, feePerByte, inputs)
		if !ok {
			check(errors.New("insufficient funds"), "Could not create transaction")
		}
		donation, change = change, types.ZeroCurrency
	}
	if !donation.IsZero() {
		outputs = append(outputs, types.SiacoinOutput{
			UnlockHash: donationAddr,
			Value:      donation,
		})
	}

	// add change (if there is any)
	if !change.IsZero() {
		var changeAddr types.UnlockHash
		if changeAddrStr != "" {
			err = changeAddr.LoadString(changeAddrStr)
			check(err, "Could not parse change address")
		} else {
			changeAddr = getChangeFlow(c, *ledger)
		}
		outputs = append(outputs, types.SiacoinOutput{
			Value:      change,
			UnlockHash: changeAddr,
		})
	}
	txn := types.Transaction{
		SiacoinInputs:  make([]types.SiacoinInput, len(used)),
		SiacoinOutputs: outputs,
		MinerFees:      []types.Currency{fee},
	}
	var inputSum types.Currency
	for i, in := range used {
		txn.SiacoinInputs[i] = in.SiacoinInput
		inputSum = inputSum.Add(in.Value)
	}
	fmt.Println("Transaction summary:")
	fmt.Printf("- %v input%v, totalling %v\n", len(used), plural(len(used)), currencyUnits(inputSum))
	fmt.Printf("- %v recipient%v, totalling %v\n", 1, plural(1), currencyUnits(recipSum))
	if !donation.IsZero() {
		fmt.Printf("- A donation of %v to the narwal server\n", currencyUnits(donation))
	}
	fmt.Printf("- A miner fee of %v, which is %v/byte\n", currencyUnits(fee), currencyUnits(feePerByte))
	if !change.IsZero() {
		fmt.Printf("- A change output, sending %v back to your wallet\n", currencyUnits(change))
	}
	fmt.Println()

	if sign {
		if *ledger {
			err := signFlowCold(c, &txn)
			check(err, "Could not sign transaction")
		} else {
			err := signFlowHot(c, &txn)
			check(err, "Could not sign transaction")
		}
	} else {
		fmt.Println("Transaction has not been signed. You can sign it with the 'sign' command.")
	}

	if broadcast {
		err := broadcastFlow(c, txn)
		check(err, "Could not broadcast transaction")
		return C.CString("Could not broadcast transaction")
	}

	writeTxn(sendToWallet, txn)
	if sign {
		fmt.Println("Wrote signed transaction to", sendToWallet)
	} else {
		fmt.Println("Wrote unsigned transaction to", sendToWallet)
	}
	return C.CString("transaction wrote")
}
